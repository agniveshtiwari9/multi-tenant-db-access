package com.mylib.teantmgmt.service;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mylib.tenantmgmt.domain.Person;

public interface PersonRepository extends MongoRepository<Person, String> {

}

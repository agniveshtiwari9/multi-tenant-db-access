package com.mylib.tenantmgmt;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.client.MongoClients;
import com.mylib.teantmgmt.service.PersonRepository;
import com.mylib.tenantmgmt.domain.Person;
import com.mylib.tenantmgmt.mongo.MultiTenantMongoDbFactory;

@Configuration
@EnableAutoConfiguration
@EnableMongoRepositories("com.nec.teantmgmt.service")
public class Application implements CommandLineRunner {

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	PersonRepository personRepository;

	@Bean
	public MongoTemplate mongoTemplate() throws Exception {
		return new MongoTemplate(mongoDbFactory());
	}

	@Bean
	public MultiTenantMongoDbFactory mongoDbFactory() throws Exception {
		return new MultiTenantMongoDbFactory(MongoClients.create(), "test");
	}

//    just to make it as simple as possible.
	private Person createPerson(final String name, final String surename, final long age) {
		Person p = new Person();
		p.setName(name);
		p.setSurname(surename);
		p.setAge(age);
		return p;
	}

	@Override
	public void run(final String... args) throws Exception {
//        add something to the default database (test)
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("test");
		this.personRepository.save(createPerson("Phillip", "Wirth",
				ChronoUnit.YEARS.between(LocalDate.of(1992, Month.FEBRUARY, 3), LocalDate.now())));

		System.out.println("data from test: " + this.personRepository.findAll());
//        okay? fine. - lets switch the database
//        MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("test666");
//
////        should be empty
//        System.out.println("data from test666: " + this.personRepository.findAll());

		MultiTenantMongoDbFactory.setDatabaseConnection("test2", MongoClients.create());
		MultiTenantMongoDbFactory.setDatabaseNameForCurrentThread("test2");

		this.personRepository.save(createPerson("Phillip2", "Wirth",
				ChronoUnit.YEARS.between(LocalDate.of(1992, Month.FEBRUARY, 3), LocalDate.now())));

		System.out.println("data from test: " + this.personRepository.findAll());
	}

	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}

}

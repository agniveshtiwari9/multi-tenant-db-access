package com.mylib.tenantmgmt.mongo;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver;
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver.IndexDefinitionHolder;
import org.springframework.data.mongodb.core.mapping.BasicMongoPersistentEntity;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity;
import org.springframework.util.Assert;

import com.mongodb.ClientSessionOptions;
import com.mongodb.DB;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

public class MultiTenantMongoDbFactory implements MongoDbFactory {

	private final String defaultName = "test";
	private static final Logger logger = LoggerFactory.getLogger(MultiTenantMongoDbFactory.class);

	private MongoTemplate mongoTemplate;

	private static final ThreadLocal<String> dbName = new ThreadLocal<String>();
	private static final HashMap<String, Object> databaseIndexMap = new HashMap<String, Object>();

	private static final HashMap<String, SimpleMongoClientDbFactory> databaseMap = new HashMap<String, SimpleMongoClientDbFactory>();

	public MultiTenantMongoDbFactory(MongoClient mongoClient, String databaseName) {

		databaseMap.put("test", new SimpleMongoClientDbFactory(mongoClient, "test"));
		dbName.set("test");
	}

	public static void setDatabaseConnection(String name, MongoClient mongoClient) {
		databaseMap.put(name, new SimpleMongoClientDbFactory(mongoClient, name));
	}

	public void setMongoTemplate(final MongoTemplate mongoTemplate) {
		Assert.isNull(this.mongoTemplate, "You can set MongoTemplate just once");
		this.mongoTemplate = mongoTemplate;
	}

	public static void setDatabaseNameForCurrentThread(final String databaseName) {
		logger.debug("Switching to database: " + databaseName);
		dbName.set(databaseName);
	}

	public static void clearDatabaseNameForCurrentThread() {
		if (logger.isDebugEnabled()) {
			logger.debug("Removing database [" + dbName.get() + "]");
		}
		dbName.remove();
	}

	private void createIndexIfNecessaryFor(final String database) {
		if (this.mongoTemplate == null) {
			logger.error("MongoTemplate is null, will not create any index.");
			return;
		}

		boolean needsToBeCreated = false;
		synchronized (MultiTenantMongoDbFactory.class) {
			final Object syncObj = databaseIndexMap.get(database);
			if (syncObj == null) {
				databaseIndexMap.put(database, new Object());
				needsToBeCreated = true;
			}
		}
//        make sure only one thread enters with needsToBeCreated = true
		synchronized (databaseIndexMap.get(database)) {
			if (needsToBeCreated) {
				logger.debug("Creating indices for database name=[" + database + "]");
				createIndexes();
				logger.debug("Done with creating indices for database name=[" + database + "]");
			}
		}
	}

	private void createIndexes() {
		final MongoMappingContext mappingContext = (MongoMappingContext) this.mongoTemplate.getConverter()
				.getMappingContext();
		final MongoPersistentEntityIndexResolver indexResolver = new MongoPersistentEntityIndexResolver(mappingContext);
		for (BasicMongoPersistentEntity<?> persistentEntity : mappingContext.getPersistentEntities()) {
			checkForAndCreateIndexes(indexResolver, persistentEntity);
		}
	}

	private void checkForAndCreateIndexes(final MongoPersistentEntityIndexResolver indexResolver,
			final MongoPersistentEntity<?> entity) {
//        make sure its a root document
		if (entity.findAnnotation(Document.class) != null) {
			for (IndexDefinitionHolder indexDefinitionHolder : indexResolver.resolveIndexForEntity(entity)) {
//                work because of javas reentered lock feature
				this.mongoTemplate.indexOps(entity.getType()).ensureIndex(indexDefinitionHolder);
			}
		}
	}

	@Override
	public MongoDatabase getDb() throws DataAccessException {
		// TODO Auto-generated method stub

		final String tlName = dbName.get();
		final String dbToUse = (tlName != null ? tlName : this.defaultName);
		logger.debug("Acquiring database: " + dbToUse);
		createIndexIfNecessaryFor(dbToUse);
		return databaseMap.get(dbName.get()).getDb();
	}

	@Override
	public MongoDatabase getDb(String dbName) throws DataAccessException {
		// TODO Auto-generated method stub
		final String tlName = dbName;
		final String dbToUse = (tlName != null ? tlName : this.defaultName);
		logger.debug("Acquiring database: " + dbToUse);
		createIndexIfNecessaryFor(dbToUse);
		return databaseMap.get(dbName).getDb(dbName);
		// return null;
	}

	@Override
	public PersistenceExceptionTranslator getExceptionTranslator() {
		// TODO Auto-generated method stub
		System.out.println(dbName.get());
		return databaseMap.get(dbName.get()).getExceptionTranslator();
	}

	@Override
	public DB getLegacyDb() {
		// TODO Auto-generated method stub
		return databaseMap.get(dbName.get()).getLegacyDb();
	}

	@Override
	public ClientSession getSession(ClientSessionOptions options) {
		// TODO Auto-generated method stub
		return databaseMap.get(dbName.get()).getSession(options);
	}

	@Override
	public MongoDbFactory withSession(ClientSession session) {
		// TODO Auto-generated method stub
		return databaseMap.get(dbName.get()).withSession(session);
	}
}
